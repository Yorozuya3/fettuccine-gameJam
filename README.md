# VitaEterna
Repositorio para el juego VitaEterna creado durante la GlobalGameJam 2020.

## Descarga del juego
Puedes encontrar el juego para descargar aqui:
https://creativwhisper.itch.io/vita-eterna 

## Creadores

* Arte: Antonia Escamilla
* Texto: Eduardo Sánchez
* Programación: Javier Montes


<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/">Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License</a>.
